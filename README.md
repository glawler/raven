# Raven

**R**y's **A**pparatus for **V**irtual **E**ncodable **N**etworks

Raven is a tool for rapidly designing, deploying and managing virtual networks. Raven networks are:
- designed programatically through a javascript API
- managed through a command line interface
- materialized and deployed by a libvirt enabled backend

When you need more infrastructure level fidelity than a Kubernetes deployment and more programmability than a collection of Vagrantfiles, raven may be of use to you.

Here is an example of a network model

```javascript
zwitch = {
  'name': 'nimbus',
  'image': 'cumulusvx-3.5',
  'os': 'linux',
  'mounts': [{ 'source': env.PWD, 'point': '/tmp/here' }]
};

nodes = Range(2).map(i => ({
  'name': `n${i}`,
  'image': 'debian-stretch',
  'os': 'linux',
  'mounts': [{ 'source': env.HOME, 'point': '/tmp/home'}]
}));

links = [
  ...Range(2).map(i => Link(`n${i}`, 'eth0', 'nimbus', `swp${i+3}`)),
]

topo = {
  'name': '2net',
  'nodes':[...nodes],
  'switches': [zwitch],
  'links': links
};
```

## Getting started
Raven has been tested on 

- Fedora 27, 28 
- Ubuntu 16.04, 18.04 
- Debian stretch, buster

Contributions to support other distros welcome!

### Installing
You'll need to have [Ansible](https://docs.ansible.com/ansible/latest/index.html) installed on your system. SELinux systems (Fedora) will also need the SELinux python bindings. Raven is built primarily in Go and must be built from your systems `GOPATH`.

```shell
git clone git@gitlab.com:rygoo/raven
cd raven

# Ensure your distros packages are up to date before running this!
sudo ansible-playbook setup.yml

```

**Warning: this process installs raven from the binary produced by the CI server associated with this repository. It is known to work for Ubuntu 18.04, Debian Buster and Fedora 28 but may not work on older platforms.**

### iptables / nftables

`nftables` is beginning it's takeover. This is happening in debian-buster.
Systems running both the iptables and nftables kernel modules can run into
troubles. Connectivity issues have been observed on systems with both sets of
kernel modules running. One remedy for this is `sudo rmmod ip_tables --force`.
Removing the `iptable_nat` module may also be necessary. Symptoms include

- no external nat
- no connectivity between hosts

#### SELinux Considerations
If you are running an SELinux system, you will need to make sure that any directory you run raven from is traversable by `root`. On Fedora this means `chmod a+x $HOME` if you are running from within your home directory. Home directories on SELinux based systems are typically not traversable by root.

### Building
You will need at least Go 1.8 to build along with the Go [dep](https://golang.github.io/dep/). Then from this directory
```
dep ensure
make
sudo make install
```

### Tinkering
First start the raven application (you must be root due to the way we use libvirt)

```shell
sudo su
cd raven/models/2net

# build the raven system (creates virtual machines and network descriptions)
rvn build

# deploy the virtual system
rvn deploy

# show the status of the virtual nodes
rvn status

# wait for the virtual nodes and switches to come up
rvn pingwait nimbus n0 n1

# show the status of the virtual nodes now that they are up 
# (you will see IP addresses)
rvn status

# configure the virtual nodes and switches
rvn configure

# while configure is running, you can open up another shell window and type in
# rvn status to see how things are progressing

# run some ad-hoc config on a node
rvn ansible n1 config/n1.yml

# ssh into a node
eval $(rvn ssh n0)

# ping n1 from n0 through the switch nimbus (see config/n1.yml) 
# to see how IP addresses re set up
rvn@n0: ping 10.47.0.2
```

To run a full build, test deploy cycle run the following.

```shell
./launch.sh
```

The [launch.sh](models/2net/launch.sh) script is fairly self explanatory and concretely shows many of the capabilities of raven.


prefix := $(or ${prefix},${prefix},/usr/local/bin)

all: \
	build/rvn

build/rvn: rvn-cli/rvn.go rvn/*.go Makefile | build
	CGO_CFLAGS="-Wno-deprecated-declarations" go build -o build/rvn rvn-cli/rvn.go

build:
	mkdir build

clean:
	rm -rf build

install: build/rvn
	sudo cp build/rvn $(prefix)/rvn

# To link with local lbs use this to build
#CGO_LDFLAGS="-L /usr/local/lib" go build -o build/rvn rvn-cli/rvn.go
